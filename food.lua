
local S = farming.translate
local a = farming.recipe_items

-- sliced bread

minetest.register_craftitem("farming:bread_slice", {
	description = S("Sliced Bread"),
	inventory_image = "farming_bread_slice.png",
	on_use = minetest.item_eat(1),
	groups = {food_bread_slice = 1, flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "farming:bread_slice 5",
	recipe = {{"group:food_bread", a.cutting_board}},
	replacements = {{"group:food_cutting_board", "farming:cutting_board"}}
})

-- toast

minetest.register_craftitem("farming:toast", {
	description = S("Toast"),
	inventory_image = "farming_toast.png",
	on_use = minetest.item_eat(1),
	groups = {food_toast = 1, flammable = 2, compostability = 65}
})

minetest.register_craft({
	type = "cooking",
	cooktime = 3,
	output = "farming:toast",
	recipe = "farming:bread_slice"
})

-- toast sandwich

minetest.register_craftitem("farming:toast_sandwich", {
	description = S("Toast Sandwich"),
	inventory_image = "farming_toast_sandwich.png",
	on_use = minetest.item_eat(4),
	groups = {flammable = 2, compostability = 85}
})

minetest.register_craft({
	output = "farming:toast_sandwich",
	recipe = {
		{"farming:bread_slice"},
		{"farming:toast"},
		{"farming:bread_slice"}
	}
})

-- filter sea water into river water

minetest.register_craft({
	output = a.bucket_river_water,
	recipe = {
		{"farming:hemp_fibre"},
		{"farming:hemp_fibre"},
		{a.bucket_water}
	}
})

if farming.mcl then

	minetest.register_craft({
		output = "mcl_potions:river_water",
		recipe = {
			{"farming:hemp_fibre"},
			{"mcl_potions:water"}
		}
	})
end

-- glass of water

minetest.register_craftitem("farming:glass_water", {
	description = S("Glass of Water"),
	inventory_image = "farming_water_glass.png",
	groups = {food_glass_water = 1, flammable = 3, vessel = 1}
})

minetest.register_craft({
	output = "farming:glass_water 4",
	recipe = {
		{a.drinking_glass, a.drinking_glass},
		{a.drinking_glass, a.drinking_glass},
		{a.bucket_river_water, ""}
	},
	replacements = {{a.bucket_river_water, a.bucket_empty}}
})

minetest.register_craft({
	output = "farming:glass_water 4",
	recipe = {
		{a.drinking_glass, a.drinking_glass},
		{a.drinking_glass, a.drinking_glass},
		{a.bucket_water, "farming:hemp_fibre"}
	},
	replacements = {{a.bucket_water, a.bucket_empty}}
})

if minetest.get_modpath("bucket_wooden") then

	minetest.register_craft({
		output = "farming:glass_water 4",
		recipe = {
			{a.drinking_glass, a.drinking_glass},
			{a.drinking_glass, a.drinking_glass},
			{"group:water_bucket_wooden", "farming:hemp_fibre"}
		},
		replacements = {{"group:water_bucket_wooden", "bucket_wooden:bucket_empty"}}
	})
end

-- Salt

minetest.register_node("farming:salt", {
	description = S("Salt"),
	inventory_image = "farming_salt.png",
	wield_image = "farming_salt.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	tiles = {"farming_salt.png"},
	groups = {food_salt = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},
	sounds = farming.sounds.node_sound_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	type = "cooking",
	cooktime = 15,
	output = "farming:salt",
	recipe = a.bucket_water,
	replacements = {{a.bucket_water, a.bucket_empty}}
})

if farming.cucina_vegana then
-- Mayonnaise

minetest.register_node("farming:mayonnaise", {
	description = S("Mayonnaise"),
	drawtype = "plantlike",
	tiles = {"farming_mayo.png"},
	inventory_image = "farming_mayo.png",
	wield_image = "farming_mayo.png",
	paramtype = "light",
	is_ground_content = false,
	walkable = false,
	on_use = minetest.item_eat(3),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.45, 0.25}
	},
	groups = {
		compostability = 65, food_mayonnaise = 1, vessel = 1, dig_immediate = 3,
		attached_node = 1
	},
	sounds = farming.sounds.node_sound_glass_defaults()
})

minetest.register_craft({
	output = "farming:mayonnaise",
	recipe = {
		{"group:food_olive_oil", "group:food_lemon"},
		{"group:food_egg", "farming:salt"}
	},
	replacements = {{"farming:olive_oil", a.glass_bottle}}
})
end

-- Rose Water

minetest.register_node("farming:rose_water", {
	description = S("Rose Water"),
	inventory_image = "farming_rose_water.png",
	wield_image = "farming_rose_water.png",
	drawtype = "plantlike",
	visual_scale = 0.8,
	paramtype = "light",
	tiles = {"farming_rose_water.png"},
	groups = {
		food_rose_water = 1, vessel = 1, dig_immediate = 3, attached_node = 1
	},
	sounds = farming.sounds.node_sound_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	}
})

minetest.register_craft({
	output = "farming:rose_water",
	recipe = {
		{a.rose, a.rose, a.rose},
		{a.rose, a.rose, a.rose},
		{"group:food_glass_water", a.pot, a.glass_bottle}
	},
	replacements = {
		{"group:food_glass_water", a.drinking_glass},
		{"group:food_pot", "farming:pot"}
	}
})

-- Turkish Delight

minetest.register_craftitem("farming:turkish_delight", {
	description = S("Turkish Delight"),
	inventory_image = "farming_turkish_delight.png",
	groups = {flammable = 3, compostability = 85},
	on_use = minetest.item_eat(2)
})

minetest.register_craft({
	output = "farming:turkish_delight 4",
	recipe = {
		{"group:food_gelatin", "group:food_sugar", "group:food_gelatin"},
		{"group:food_sugar", "group:food_rose_water", "group:food_sugar"},
		{"group:food_sugar", a.dye_pink, "group:food_sugar"}
	},
	replacements = {
		{"group:food_cornstarch", a.bowl},
		{"group:food_cornstarch", a.bowl},
		{"group:food_rose_water", a.glass_bottle}
	}
})

-- Garlic Bread

minetest.register_craftitem("farming:garlic_bread", {
	description = S("Garlic Bread"),
	inventory_image = "farming_garlic_bread.png",
	groups = {flammable = 3, compostability = 65},
	on_use = minetest.item_eat(2)
})

minetest.register_craft({
	output = "farming:garlic_bread",
	recipe = {
		{"group:food_toast", "group:food_garlic_clove", "group:food_garlic_clove"}
	}
})

-- Donuts (thanks to Bockwurst for making the donut images)

minetest.register_craftitem("farming:donut", {
	description = S("Donut"),
	inventory_image = "farming_donut.png",
	on_use = minetest.item_eat(4),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:donut 3",
	recipe = {
		{"", "group:food_flour", ""},
		{"group:food_flour", "group:food_sugar", "group:food_flour"},
		{"", "group:food_flour", ""},
	}
})

minetest.register_craftitem("farming:donut_chocolate", {
	description = S("Chocolate Donut"),
	inventory_image = "farming_donut_chocolate.png",
	on_use = minetest.item_eat(6),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:donut_chocolate",
	recipe = {
		{"group:food_cocoa"},
		{"farming:donut"}
	}
})

minetest.register_craftitem("farming:donut_apple", {
	description = S("Apple Donut"),
	inventory_image = "farming_donut_apple.png",
	on_use = minetest.item_eat(6),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:donut_apple",
	recipe = {
		{"group:food_apple"},
		{"farming:donut"}
	}
})

-- Porridge Oats

minetest.register_craftitem("farming:porridge", {
	description = S("Porridge"),
	inventory_image = "farming_porridge.png",
	on_use = minetest.item_eat(6, a.bowl),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:porridge",
	recipe = {
		{"group:food_oats", "group:food_oats", "group:food_oats"},
		{"group:food_oats", "group:food_bowl", "group:food_milk_glass"}
	},
	replacements = {
		{"mobs:glass_milk", a.drinking_glass},
		{"farming:soy_milk", a.drinking_glass}
	}
})

-- Jaffa Cake

minetest.register_craftitem("farming:jaffa_cake", {
	description = S("Jaffa Cake"),
	inventory_image = "farming_jaffa_cake.png",
	on_use = minetest.item_eat(6),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:jaffa_cake 3",
	recipe = {
		{a.baking_tray, "group:food_egg", "group:food_sugar"},
		{a.flour, "group:food_cocoa", "group:food_orange"},
		{"group:food_milk", "", ""}
	},
	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
		{"mobs:bucket_milk", a.bucket_empty},
		{"mobs:wooden_bucket_milk", "wooden_bucket:bucket_wood_empty"},
		{"farming:soy_milk", a.drinking_glass}
	}
})

-- Whipping Cream

minetest.register_craftitem("farming:whipping_cream", {
	description = S("Whipping Cream"),
	inventory_image = "farming_whipping_cream.png",
	on_use = minetest.item_eat(4, "vessels:glass_bottle"),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:whipping_cream",
	recipe = {"petz:bucket_milk", "farming:mixing_bowl", "vessels:glass_bottle"},
       	replacements = {
                {"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	},
})
-- Tea

-- Mint Tea

minetest.register_node("farming:tea_mint", {
	description = S("Mint Tea"),
	inventory_image = "farming_tea_mint.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_mint.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_mint_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:tea_mint",
	recipe = {"vessels:drinking_glass", "africaplants:mint", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},

})

-- Nettle Tea

minetest.register_node("farming:tea_nettle", {
	description = S("Nettle Tea"),
	inventory_image = "farming_tea_nettle.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_nettle.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_nettle_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:tea_nettle",
	recipe = {"vessels:drinking_glass", "nettle:nettle", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},

})

-- Orange Tea

minetest.register_node("farming:tea_orange", {
	description = S("Orange Tea"),
	inventory_image = "farming_tea_orange.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_orange.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_orange_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {vessel = 1, dig_immediate = 3, attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.25, 0.25}
	},
})

minetest.register_craft({
	output = "farming:tea_orange",
	recipe = {
		{"", "ethereal:orange", "africaplants:star_anise_plant"}, 
		{"", "bucket:bucket_water", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- White Tea

minetest.register_node("farming:tea_white", {
	description = S("White Tea"),
	inventory_image = "farming_tea_white.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_white.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_white_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:tea_white",
	recipe = {"vessels:drinking_glass", "farming:vanilla", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Hot Chocolate

minetest.register_node("farming:tea_hot_chocolate", {
	description = S("Hot Chocolate"),
	inventory_image = "farming_tea_hot_chocolate.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_hot_chocolate.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_hot_chocolate_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	output = "farming:tea_hot_chocolate",
	recipe = {
		{"", "farming:chocolate_dark", "farming:sugar"}, 
		{"", "petz:bucket_milk", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Strawberry Tea

minetest.register_node("farming:tea_strawberry", {
	description = S("Strawberry Tea"),
	inventory_image = "farming_tea_strawberry.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_strawberry.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_strawberry_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:tea_strawberry",
	recipe = {"vessels:drinking_glass", "ethereal:strawberry", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Honey Lemon Tea

minetest.register_node("farming:tea_honey_lemon", {
	description = S("Honey Lemon Tea"),
	inventory_image = "farming_tea_honey_lemon.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
	wield_image = "farming_tea_honey_lemon.png",
	drawtype = "torchlike",
	visual_scale = 0.8,
	paramtype = "light",
	walkable = false,
	tiles = {{
   		name = "farming_tea_honey_lemon_animated.png",
   		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
}},
	groups = {farming_tea = 1, vessel = 1, dig_immediate = 3,
			attached_node = 1},

	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 0.3, 0.25}
	},
})

minetest.register_craft({
	output = "farming:tea_honey_lemon",
	recipe = {
		{"", "lemontree:lemon", "petz:honey_bottle"}, 
		{"", "bucket:bucket_water", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
		{"petz:honey_bottle", "vessels:glass_bottle"}
	},	
})

-- Muffins 

-- Strawberry

minetest.register_craftitem("farming:muffin_strawberry", {
	description = S("Strawberry Muffin"),
	inventory_image = "farming_muffin_strawberry.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_strawberry 2",
	recipe = {"ethereal:strawberry", "group:food_bread", "ethereal:strawberry"},
})

-- Banana

minetest.register_craftitem("farming:muffin_banana", {
	description = S("Banana Muffin"),
	inventory_image = "farming_muffin_banana.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_banana 2",
	recipe = {"ethereal:banana", "group:food_bread", "ethereal:banana"},
})

-- Chocolate

minetest.register_craftitem("farming:muffin_chocolate", {
	description = S("Chocolate Muffin"),
	inventory_image = "farming_muffin_chocolate.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_chocolate 2",
	recipe = {"farming:chocolate_dark", "group:food_bread", "farming:chocolate_dark"},
})

-- Raspberries

minetest.register_craftitem("farming:muffin_raspberry", {
	description = S("Raspberry Muffin"),
	inventory_image = "farming_muffin_raspberry.png",
	on_use = minetest.item_eat(4),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:muffin_raspberry 2",
	recipe = {"farming:raspberries", "group:food_bread", "farming:raspberries"},
})
-- Pumpkin Pie

minetest.register_craftitem("farming:pumpkin_pie", {
	description = S("Pumpkin Pie"),
	inventory_image = "farming_pumpkin_pie.png",
	on_use = minetest.item_eat(14),
})

minetest.register_craft({
	output = "farming:pumpkin_pie",
	recipe = { 
		{"farming:baking_tray", "farming:sugar", "farming:whipping_cream"},
		{"farming:pumpkin_slice", "farming:pumpkin_slice", "farming:pumpkin_slice"},
		{"group:food_flour", "group:food_flour", "group:food_flour"},
	},
	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},	
		{"farming:whipping_cream", "vessels:drinking_glass"},
	},
})

-- Sugar

minetest.register_craftitem("farming:sugar", {
	description = S("Sugar"),
	inventory_image = "farming_sugar.png",
	groups = {food_sugar = 1, flammable = 3},
})

-- Brown Sugar

minetest.register_craftitem("farming:brown_sugar", {
	description = S("Brown Sugar"),
	inventory_image = "farming_brown_sugar.png",
})

minetest.register_craft({
	type = "cooking",
	cooktime = 2,
	output = "farming:sugar",
	recipe = "farming:brown_sugar",
})

minetest.register_craft({
	type = "cooking",
	cooktime = 3,
	output = "farming:brown_sugar 2",
	recipe = "default:papyrus",
})

-- Caramel

minetest.register_craftitem("farming:caramel", {
	description = S("Caramel"),
	inventory_image = "farming_caramel.png",
	on_use = minetest.item_eat(3),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:caramel",
	recipe = {"farming:brown_sugar", "farming:brown_sugar", "farming:brown_sugar", "farming:brown_sugar"},
})

-- Caramel Apple

minetest.register_craftitem("farming:caramel_apple", {
	description = S("Caramel Apple"),
	inventory_image = "farming_caramel_apple.png",
	on_use = minetest.item_eat(5, "default:stick"),
})

minetest.register_craft({
	output = "farming:caramel_apple",
	recipe = {
		{"", "default:stick", ""}, 
		{"", "default:apple", ""},
		{"", "farming:caramel", ""},	
	},	
})

-- Sugar Block

minetest.register_node("farming:sugar_block", {
	description = S("Sugar Block"),
	tiles = {"farming_sugar_block.png"},
	groups = {shovely = 1, handy = 1, crumbly = 2},
	floodable = true,
	sounds = farming.sounds.node_sound_gravel_defaults(),
	_mcl_hardness = 0.8,
	_mcl_blast_resistance = 1
})

minetest.register_craft({
	output = "farming:sugar_block",
	recipe = {
		{a.sugar, a.sugar, a.sugar},
		{a.sugar, a.sugar, a.sugar},
		{a.sugar, a.sugar, a.sugar}
	}
})

minetest.register_craft({
	output = a.sugar .. " 9",
	recipe = {{"farming:sugar_block"}}
})
	
-- Raspberry Pie

minetest.register_craftitem("farming:raspberry_pie", {
	description = S("Raspberry Pie"),
	inventory_image = "farming_raspberry_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:raspberry_pie",
	recipe = {"group:food_flour", "farming:sugar", "farming:raspberries", "farming:baking_tray"},

	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})

-- Strawberry Pie

minetest.register_craftitem("farming:strawberry_pie", {
	description = S("Strawberry Pie"),
	inventory_image = "farming_strawberry_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:strawberry_pie",
	recipe = {"group:food_flour", "farming:sugar", "ethereal:strawberry", "farming:baking_tray"},
	
	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})

-- Apple Pie

minetest.register_craftitem("farming:apple_pie", {
	description = S("Apple Pie"),
	inventory_image = "farming_apple_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:apple_pie",
	recipe = {"group:food_flour", "farming:sugar", "default:apple", "farming:baking_tray"},

	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})
-- Mangrove Apple Pie

minetest.register_craftitem("farming:mangrove_apple_pie", {
	description = S("Mangrove Apple Pie"),
	inventory_image = "farming_mangrove_apple_pie.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:mangrove_apple_pie",
	recipe = {"group:food_flour", "farming:sugar", "australia:mangrove_apple", "farming:baking_tray"},

	replacements = {
		{"farming:baking_tray", "farming:baking_tray"},
	},

})

-- Cactus Juice

minetest.register_craftitem("farming:cactus_juice", {
	description = S("Cactus Juice"),
	inventory_image = "farming_cactus_juice.png",
	groups = {vessel = 1, drink = 1, compostability = 55},

	on_use = function(itemstack, user, pointed_thing)

		if user then

			local num = math.random(5) == 1 and -1 or 2

			return minetest.do_item_eat(num, "vessels:drinking_glass",
					itemstack, user, pointed_thing)
		end
	end
})

minetest.register_craft({
	output = "farming:cactus_juice",
	recipe = {
		{a.juicer},
		{a.cactus},
		{a.drinking_glass}
	},
	replacements = {
		{"group:food_juicer", "farming:juicer"}
	}
})

-- Pasta

minetest.register_craftitem("farming:pasta", {
	description = S("Pasta"),
	inventory_image = "farming_pasta.png",
	groups = {compostability = 65, food_pasta = 1}
})

minetest.register_craft({
	output = "farming:pasta",
	recipe = {
		{a.flour, "group:food_butter", a.mixing_bowl}
	},
	replacements = {{"group:food_mixing_bowl", "farming:mixing_bowl"}}
})

minetest.register_craft({
	output = "farming:pasta",
	recipe = {
		{a.flour, "group:food_oil", a.mixing_bowl}
	},
	replacements = {
		{"group:food_mixing_bowl", "farming:mixing_bowl"},
		{"group:food_oil", a.glass_bottle}
	}
})

-- Mac & Cheese

minetest.register_craftitem("farming:mac_and_cheese", {
	description = S("Mac & Cheese"),
	inventory_image = "farming_mac_and_cheese.png",
	on_use = minetest.item_eat(6, a.bowl),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:mac_and_cheese",
	recipe = {
		{"group:food_pasta", "group:food_cheese", "group:food_bowl"}
	}
})

-- Spaghetti

minetest.register_craftitem("farming:spaghetti", {
	description = S("Spaghetti"),
	inventory_image = "farming_spaghetti.png",
	on_use = minetest.item_eat(8),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:spaghetti",
	recipe = {
		{"group:food_pasta", "group:food_tomato", a.saucepan},
		{"group:food_garlic_clove", "group:food_garlic_clove", ""}
	},
	replacements = {{"group:food_saucepan", "farming:saucepan"}}
})

-- Korean Bibimbap

minetest.register_craftitem("farming:bibimbap", {
	description = S("Bibimbap"),
	inventory_image = "farming_bibimbap.png",
	on_use = minetest.item_eat(8, a.bowl),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:bibimbap",
	recipe = {
		{a.skillet, "group:food_bowl", "group:food_egg"},
		{"group:food_rice", "group:food_chicken_raw", "group:food_cabbage"},
		{"group:food_carrot", "group:food_chili_pepper", ""}
	},
	replacements = {{"group:food_skillet", "farming:skillet"}}
})

minetest.register_craft({
	output = "farming:bibimbap",
	type = "shapeless",
	recipe = {
		a.skillet, "group:food_bowl", "group:food_mushroom",
		"group:food_rice", "group:food_cabbage", "group:food_carrot",
		"group:food_mushroom", "group:food_chili_pepper"
	},
	replacements = {{"group:food_skillet", "farming:skillet"}}
})

-- Burger

minetest.register_craftitem("farming:burger", {
	description = S("Burger"),
	inventory_image = "farming_burger.png",
	on_use = minetest.item_eat(16),
	groups = {compostability = 95}
})

minetest.register_craft({
	output = "farming:burger",
	recipe = {
		{a.bread, "group:food_meat", "group:food_cheese"},
		{"group:food_tomato", "group:food_cucumber", "group:food_onion"},
		{"group:food_lettuce", "", ""}
	}
})

minetest.register_alias("burger:classic_burger", "farming:burger")
minetest.register_alias("burger:cheeseburger", "farming:burger")
minetest.register_alias("burger:double_whopper", "farming:double_whopper")
minetest.register_alias("burger:hotdog", "farming:burger")

minetest.register_craftitem("farming:double_whopper", {
	description = ("Double Whopper"),
	inventory_image = "farming_double_whopper.png",
	on_use = minetest.item_eat(20)
})

minetest.register_craft({
	output = "farming:double_whopper",
	recipe = {
		{a.bread, "group:food_meat", "group:food_cheese"},
		{"group:food_tomato", "group:food_meat", "group:food_onion"},
		{"group:food_lettuce", "group:food_cucumber", ""}
	}
})

-- Ketchup Bottle

minetest.register_craftitem("farming:ketchup_bottle", {
    description = ("Ketchup Bottle"),
    inventory_image = "farming_ketchup_bottle.png",
    on_use = minetest.item_eat(2, "vessels:glass_bottle"),
})

minetest.register_craft({
    type = "shapeless",
    output = "farming:ketchup_bottle",
    recipe = {"farming:tomato", "farming:sugar", "vessels:glass_bottle"},
})

-- Salad

minetest.register_craftitem("farming:salad", {
	description = S("Salad"),
	inventory_image = "farming_salad.png",
	on_use = minetest.item_eat(8, a.bowl),
	groups = {compostability = 45}
})

minetest.register_craft({
	output = "farming:salad",
	type = "shapeless",
	recipe = {
		"group:food_bowl", "group:food_tomato", "group:food_cucumber",
		"group:food_lettuce", "group:food_oil"
	}
})

-- Triple Berry Smoothie

minetest.register_craftitem("farming:smoothie_berry", {
	description = S("Triple Berry Smoothie"),
	inventory_image = "farming_berry_smoothie.png",
	on_use = minetest.item_eat(6, "vessels:drinking_glass"),
	groups = {vessel = 1, drink = 1, compostability = 65}
})

minetest.register_craft({
	output = "farming:smoothie_berry",
	type = "shapeless",
	recipe = {
		"group:food_raspberries", "group:food_blackberries",
		"group:food_strawberry", "group:food_banana",
		a.drinking_glass
	}
})

-- Patatas a la importancia
if farming.parsley then
minetest.register_craftitem("farming:spanish_potatoes", {
	description = S("Spanish Potatoes"),
	inventory_image = "farming_spanish_potatoes.png",
	on_use = minetest.item_eat(8, a.bowl),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:spanish_potatoes",
	recipe = {
		{"group:food_potato", "group:food_parsley", "group:food_potato"},
		{"group:food_egg", a.flour, "group:food_onion"},
		{"farming:garlic_clove", "group:food_bowl", a.skillet}
	},
	replacements = {{"group:food_skillet", "farming:skillet"}}
})
end
if farming.cucina_vegana then
-- Potato omelet

minetest.register_craftitem("farming:potato_omelet", {
	description = S("Potato omelet"),
	inventory_image = "farming_potato_omelet.png",
	on_use = minetest.item_eat(6, a.bowl),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:potato_omelet",
	recipe = {
		{"group:food_egg", "group:food_potato", "group:food_onion"},
		{a.skillet, "group:food_bowl", ""}
	},
	replacements = {{"group:food_skillet", "farming:skillet"}}
})

-- Paella

minetest.register_craftitem("farming:paella", {
	description = S("Paella"),
	inventory_image = "farming_paella.png",
	on_use = minetest.item_eat(8, a.bowl),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:paella",
	recipe = {
		{"group:food_rice", a.dye_orange, "farming:pepper_red"},
		{"group:food_peas", "group:food_chicken", "group:food_bowl"},
		{"", a.skillet, ""}
	},
	replacements = {{"group:food_skillet", "farming:skillet"}}
})

-- Flan
minetest.register_craftitem("farming:flan", {
	description = S("Vanilla Flan"),
	inventory_image = "farming_vanilla_flan.png",
	on_use = minetest.item_eat(6),
	groups = {compostability = 65}
})

minetest.register_craft({
	output = "farming:flan",
	recipe = {
		{"group:food_sugar", "group:food_milk", "farming:caramel"},
		{"group:food_egg", "group:food_egg", "farming:vanilla_extract"}
	},
	replacements = {
		{"cucina_vegana:soy_milk", a.drinking_glass},
		{"mobs:bucket_milk", "bucket:bucket_empty"},
		{"mobs:wooden_bucket_milk", "wooden_bucket:bucket_wood_empty"},
		{"farming:vanilla_extract", a.glass_bottle}
	}
})
-- Vegan Cheese

minetest.register_craftitem("farming:cheese_vegan", {
	description = S("Vegan Cheese"),
	inventory_image = "farming_cheese_vegan.png",
	on_use = minetest.item_eat(2),
	groups = {compostability = 65, food_cheese = 1, flammable = 2}
})

minetest.register_craft({
	output = "farming:cheese_vegan",
	recipe = {
		{"farming:soy_milk", "farming:soy_milk", "farming:soy_milk"},
		{"group:food_salt", "group:food_peppercorn", "farming:bottle_ethanol"},
		{"group:food_gelatin", a.pot, ""}
	},
	replacements = {
		{"farming:soy_milk", a.drinking_glass .. " 3"},
		{"farming:pot", "farming:pot"},
		{"farming:bottle_ethanol", a.glass_bottle}
	}
})

minetest.register_craft({
	output = "farming:cheese_vegan",
	recipe = {
		{"farming:soy_milk", "farming:soy_milk", "farming:soy_milk"},
		{"group:food_salt", "group:food_peppercorn", "group:food_lemon"},
		{"group:food_gelatin", a.pot, ""}
	},
	replacements = {
		{"farming:soy_milk", a.drinking_glass .. " 3"},
		{"farming:pot", "farming:pot"}
	}
})
-- Onigiri

minetest.register_craftitem("farming:onigiri", {
	description = S("Onigiri"),
	inventory_image = "farming_onigiri.png",
	on_use = minetest.item_eat(2),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "farming:onigiri",
	recipe = {
		{"group:food_rice", "group:food_salt", "group:food_rice"},
		{"", "group:food_seaweed", ""}
	}
})
end

-- Gyoza

minetest.register_craftitem("farming:gyoza", {
	description = S("Gyoza"),
	inventory_image = "farming_gyoza.png",
	on_use = minetest.item_eat(4),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "farming:gyoza 4",
	recipe = {
		{"group:food_cabbage", "group:food_garlic_clove", "group:food_onion"},
		{"group:food_meat", "group:food_salt", a.flour},
		{"", a.skillet, ""}

	},
	replacements = {
		{"group:food_skillet", "farming:skillet"}
	}
})

-- Mochi

minetest.register_craftitem("farming:mochi", {
	description = S("Mochi"),
	inventory_image = "farming_mochi.png",
	on_use = minetest.item_eat(3),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "farming:mochi",
	recipe = {
		{"", a.mortar_pestle, ""},
		{"group:food_rice", "group:food_sugar", "group:food_rice"},
		{"", "group:food_glass_water", ""}
	},
	replacements = {
		{"group:food_mortar_pestle", "farming:mortar_pestle"},
		{"group:food_glass_water", a.drinking_glass}
	}
})

if farming.ginger then
-- Gingerbread Man
minetest.register_craftitem("farming:gingerbread_man", {
	description = S("Gingerbread Man"),
	inventory_image = "farming_gingerbread_man.png",
	on_use = minetest.item_eat(2),
	groups = {compostability = 85}
})

minetest.register_craft({
	output = "farming:gingerbread_man 3",
	recipe = {
		{"", "group:food_egg", ""},
		{"group:food_flour", "group:food_ginger", "group:food_flour"},
		{"group:food_sugar", "", "group:food_sugar"}
	}
})
end

-- Baguette Dough

minetest.register_craftitem("farming:dough_baguette", {
        description = S("Baguette Dough"),
        inventory_image = "farming_dough_baguette.png",
        groups = {food_dough = 1},
})

minetest.register_craft({
        output = "farming:dough_baguette",
        recipe = {
                {"farming:flour", "group:food_egg", ""},
                {"", "bucket:bucket_water", "farming:flour_multigrain"},
                {"", "", "farming:mixing_bowl"}
       	},
       	replacements = {
                {"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	}
})

minetest.register_craft({
        output = "farming:dough_baguette",
        recipe = {
                {"farming:flour", "group:food_egg", ""},
                {"", "bucket:bucket_river_water", "farming:flour_multigrain"},
                {"", "", "farming:mixing_bowl"}
       	},
       	replacements = {
		{"bucket:bucket_river_water", "bucket:bucket_empty"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	}
})

-- Baguette

minetest.register_craftitem("farming:baguette", {
	description = S("Baguette"),
	inventory_image = "farming_baguette.png",
	on_use = minetest.item_eat(14),
})

minetest.register_craft({
	type = "cooking",
	cooktime = 10,
	output = "farming:baguette",
	recipe = "farming:dough_baguette",
})

-- Croissant Dough

minetest.register_craftitem("farming:dough_croissant", {
	description = S("Croissant Dough"),
	inventory_image = "farming_dough_croissant.png",
	groups = {food_dough = 1},
})

minetest.register_craft({
	output = "farming:dough_croissant",
	recipe = {
		{"", "petz:honey_bottle", ""},
		{"farming:flour", "", "farming:flour"},
		{"group:food_milk", "", "farming:mixing_bowl"}
  	},
       	replacements = {
                {"group:food_milk", "bucket:bucket_empty"},
		{"petz:honey_bottle", "vessels:glass_bottle"},
		{"farming:mixing_bowl", "farming:mixing_bowl"},
	}
})

-- Croissant

minetest.register_craftitem("farming:croissant", {
	description = S("Croissant"),
	inventory_image = "farming_croissant.png",
	on_use = minetest.item_eat(12),
})

minetest.register_craft({
	type = "cooking",
	cooktime = 10,
	output = "farming:croissant",
	recipe = "farming:dough_croissant",
})

-- Strawberry Shortcake 

minetest.register_craftitem("farming:strawberry_shortcake", {
	description = S("Strawberry Shortcake"),
	inventory_image = "farming_strawberry_shortcake.png",
	on_use = minetest.item_eat(16),
})

minetest.register_craft({
	output = "farming:strawberry_shortcake",
	recipe = {
		{"ethereal:strawberry", "farming:whipping_cream", "ethereal:strawberry"},
		{"farming:sugar", "farming:flour", "farming:sugar"},
		{"farming:flour", "petz:bucket_milk", "farming:flour"},
	},
       	replacements = {
                {"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:whipping_cream", "vessels:glass_bottle"},
	},
})
-- Tea

-- Mint Tea

minetest.register_craftitem("farming:tea_mint", {
	description = S("Mint Tea"),
	inventory_image = "farming_tea_mint.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
})

minetest.register_craft({ 
	type = "shapeless",
	output = "farming:tea_mint",
	recipe = {"vessels:drinking_glass", "africaplants:mint", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},

})

-- Orange Tea

minetest.register_craftitem("farming:tea_orange", {
	description = S("Orange Tea"),
	inventory_image = "farming_tea_orange.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
})

minetest.register_craft({
	output = "farming:tea_orange",
	recipe = {
		{"", "ethereal:orange", "africaplants:star_anise_plant"}, 
		{"", "bucket:bucket_water", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- White Tea

minetest.register_craftitem("farming:tea_white", {
	description = S("White Tea"),
	inventory_image = "farming_tea_white.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:tea_white",
	recipe = {"vessels:drinking_glass", "farming:vanilla", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Hot Chocolate

minetest.register_craftitem("farming:tea_hot_chocolate", {
	description = S("Hot Chocolate"),
	inventory_image = "farming_tea_hot_chocolate.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
})

minetest.register_craft({
	output = "farming:tea_hot_chocolate",
	recipe = {
		{"", "farming:chocolate_dark", "farming:sugar"}, 
		{"", "petz:bucket_milk", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"petz:bucket_milk", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Strawberry Tea

minetest.register_craftitem("farming:tea_strawberry", {
	description = S("Strawberry Tea"),
	inventory_image = "farming_tea_strawberry.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:tea_strawberry",
	recipe = {"vessels:drinking_glass", "ethereal:strawberry", "bucket:bucket_water", "farming:saucepan"},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
	},
})

-- Honey Lemon Tea

minetest.register_craftitem("farming:tea_honey_lemon", {
	description = S("Honey Lemon Tea"),
	inventory_image = "farming_tea_honey_lemon.png",
	on_use = minetest.item_eat(4, "vessels:drinking_glass"),
})

minetest.register_craft({
	output = "farming:tea_honey_lemon",
	recipe = {
		{"", "lemontree:lemon", "petz:honey_bottle"}, 
		{"", "bucket:bucket_water", "farming:saucepan"},
		{"", "", "vessels:drinking_glass"},
	},
	replacements = {
		{"bucket:bucket_water", "bucket:bucket_empty"},
		{"farming:saucepan", "farming:saucepan"},
		{"petz:honey_bottle", "vessels:glass_bottle"},
	},
})
